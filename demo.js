// onload 事件会在页面加载完发生
window.onload = () => {
  waterFall()

  // 监听窗口调整大小
  window.onresize = throttle(() => {
    waterFall()
  }, 300)

  let con = document.getElementById('con')

  // 监听浏览器滚动条滚动
  document.onscroll = throttle(() => {
    let item = document.getElementsByClassName('item')
    if (
      item[item.length - 1].getBoundingClientRect().top <
      window.innerHeight
    ) {
      // console.log('load');
      // innerHTML会触发原DOM节点下所有内容的重排，appendChild只会触发部分重排。
      for (let i = 0; i < 4; i++) {
        let itemHeight = Math.floor(getRandomHeight())
        let itemWidth =
          document.documentElement.clientWidth
        let color = randomColor()
        let src = randomString(getRandomInt(10, 30))
        let box = document.createElement('div')
        let img = document.createElement('img')
        let p = document.createElement('p')
        box.className = 'item'
        box.style.width = `${itemWidth}`
        box.style.height = `${itemHeight}`
        img.src = `https://via.placeholder.com/${Math.floor(itemWidth / 4)}x${Math.floor(itemHeight / 4)}/${color}`
        p.innerText = `${src}`
        box.appendChild(img)
        box.appendChild(p)
        con.appendChild(box)
        // con.innerHTML += `<div class="item">
        //                     <img src="https://via.placeholder.com/${itemWidth}x${itemHeight}/${color}"  />
        //                     <p>${src}</p>
        //                   </div>`;
      }
    }
    waterFall()
  }, 300)
}

// 瀑布流布局
const waterFall = () => {
  let item = document.getElementsByClassName('item')
  let clientWidth = document.documentElement.clientWidth
  let hrr = []
  for (i = 0; i < item.length; i++) {
    // 两栏布局
    if (i < 2) {
      item[i].style.top = 0
      item[i].style.left = i * item[0].offsetWidth + 'px'
      hrr.push(item[i].offsetHeight)
    } else {
      let min = Math.min(...hrr)
      let index = hrr.indexOf(min)
      item[i].style.top = min + 'px'
      item[i].style.left = index * item[0].offsetWidth + 'px'
      hrr[index] += item[i].offsetHeight
    }
  }
  // console.log(hrr);
}

// 节流
const throttle = (fn, space) => {
  let task = null
  return function () {
    if (!task) {
      task = setTimeout(function () {
        task = null
        fn.apply(this, arguments)
      }, space)
    }
  }
}

// 获取指定范围的随机数
const getRandomInt = (min, max) => {
  return Math.floor(Math.random() * (max - min + 1)) + min
}
// 获取随机高度，另外高度最小的2个盒子高度相加不能小于可视高度
const getRandomHeight = () => {
  let clientHeight = document.documentElement.clientHeight
  // console.log(clientHeight)
  return getRandomInt(clientHeight / 2, clientHeight)
}
// 获取随机颜色值
const randomColor = () => {
  let color = ''
  for (var i = 0; i < 6; i++)
    color += parseInt(Math.random() * 16).toString(16)
  return color
}
// 获取随机字符串
const randomString = (length) => {
  var str =
    '君不见，黄河之水天上来，奔流到海不复回。君不见，高堂明镜悲白发，朝如青丝暮成雪。人生得意须尽欢，莫使金樽空对月。天生我材必有用，千金散尽还复来。烹羊宰牛且为乐，会须一饮三百杯。岑夫子，丹丘生，将进酒，杯莫停。与君歌'
  var result = ''
  for (var i = length; i > 0; --i)
    result += str[Math.floor(Math.random() * str.length)]
  return result
}